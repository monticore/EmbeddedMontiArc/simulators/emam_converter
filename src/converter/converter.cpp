/* (c) https://github.com/MontiCore/monticore */
#include "converter.hpp"

namespace emam_converter {

    Converter::Converter(ros::NodeHandle node_handle, ros::NodeHandle private_node_handle) : params_{
            private_node_handle} {
        //Init parameters
        params_.fromParamServer();

        std::regex indexRegex("\\{i\\}");
        //Publisher
        for (unsigned int i = 1; i <= n; i++) {
            const std::string &indexString = std::to_string(i);

            std::string absTrajTopic = std::regex_replace(params_.abs_trajectory_topic_template, indexRegex,
                                                          indexString);
            absTrajPubs.push_back(
                    node_handle.advertise<std_msgs::Float64MultiArray>(absTrajTopic, params_.msg_queue_size));

            std::string dtTopic = std::regex_replace(params_.delta_time_topic_template, indexRegex, indexString);
            dtPubs.push_back(node_handle.advertise<std_msgs::Float64>(dtTopic, params_.msg_queue_size));

            std::string collisionHullsTopic = std::regex_replace(params_.collision_hulls_topic_template, indexRegex,
                                                                 indexString);
            hullPubs.push_back(node_handle.advertise<struct_msgs::ba_struct_Rectangle>(collisionHullsTopic,
                                                                                       params_.msg_queue_size));
        }

        //Subscriber + fields
        for (uint i = 1; i <= n; i++) {
            std::string curVelTopic = std::regex_replace(params_.current_velocity_topic_template, indexRegex,
                                                         std::to_string(i));
            auto callback = boost::bind(&Converter::curVelCallback, this, _1, i);
            curVelSub.push_back(
                    node_handle.subscribe<std_msgs::Float64>(curVelTopic, params_.msg_queue_size, callback));

            curDtMap[i] = 0;
        }


        auto groundTruthTopic = params_.ground_truth_topic;
        groundTruthSub = node_handle.subscribe(groundTruthTopic, params_.msg_queue_size,
                                               &Converter::groundTruthCallback, this,
                                               ros::TransportHints().tcpNoDelay());

        auto desiredMotionTopic = params_.desired_motion_topic;
        desiredMotionSub = node_handle.subscribe(desiredMotionTopic, params_.msg_queue_size,
                                                 &Converter::desiredMotionCallback, this,
                                                 ros::TransportHints().tcpNoDelay());
    }

    void Converter::publishCollisionHulls(
            const automated_driving_msgs::ObjectStateArray_<std::allocator<void>>::ConstPtr &msg) {//check collision
        std::vector<Rect> hulls;
        for (auto object : msg->objects) {
            Rect tmpRect = Rect(object.hull);

            tf::Quaternion q(object.motion_state.pose.pose.orientation.x, object.motion_state.pose.pose.orientation.y,
                             object.motion_state.pose.pose.orientation.z, object.motion_state.pose.pose.orientation.w);
            tmpRect.rotateAndTranslate(object.motion_state.pose.pose.position.x,
                                       object.motion_state.pose.pose.position.y, q.getAngle());
            hulls.push_back(tmpRect);
        }

        for (uint i = 0; i < hulls.size(); i++) {
            //Q^{2,4}
//            std_msgs::Float64MultiArray hullMsg;
//            hullMsg.data.resize(2 * 4);
//            hullMsg.data[0 * 4 + 0] = hulls[i].leftBot.x;
//            hullMsg.data[1 * 4 + 0] = hulls[i].leftBot.y;
//
//            hullMsg.data[0 * 4 + 1] = hulls[i].rightBot.x;
//            hullMsg.data[1 * 4 + 1] = hulls[i].rightBot.y;
//
//            hullMsg.data[0 * 4 + 2] = hulls[i].rightTop.x;
//            hullMsg.data[1 * 4 + 2] = hulls[i].rightTop.y;
//
//            hullMsg.data[0 * 4 + 3] = hulls[i].leftTop.x;
//            hullMsg.data[1 * 4 + 3] = hulls[i].leftTop.y;
            struct_msgs::ba_struct_Rectangle hullMsg;
            hullMsg.pointA.posX = hulls[i].leftBot.x;
            hullMsg.pointA.posY = hulls[i].leftBot.y;

            hullMsg.pointB.posX = hulls[i].rightBot.x;
            hullMsg.pointB.posY = hulls[i].rightBot.y;

            hullMsg.pointC.posX = hulls[i].rightTop.x;
            hullMsg.pointC.posY = hulls[i].rightTop.y;

            hullMsg.pointD.posX = hulls[i].leftTop.x;
            hullMsg.pointD.posY = hulls[i].leftTop.y;

            hullPubs[i].publish(hullMsg);
        }
    }


    void Converter::curVelCallback(const std_msgs::Float64::ConstPtr &msg, unsigned int id) {
        //reset dt for this id as the corrisponding ba_system_velocityController_ published a velocity
        curDtMap[id] = 0;

        //send the current velocity to the planner via the dynamic_reconfigure service
        dynamic_reconfigure::Config conf;
        dynamic_reconfigure::DoubleParameter doubleParameter;
        dynamic_reconfigure::ReconfigureRequest reconfigureRequest;
        dynamic_reconfigure::ReconfigureResponse reconfigureResponse;

        doubleParameter.value = msg->data > 0 ? msg->data : 0.01;
        doubleParameter.name = "v_desired";
        conf.doubles.push_back(doubleParameter);

        reconfigureRequest.config = conf;

        ros::service::call("/v" + std::to_string(id) + "/planning/planner/set_parameters", reconfigureRequest,
                           reconfigureResponse);
    }

    void Converter::groundTruthCallback(const automated_driving_msgs::ObjectStateArray::ConstPtr &msg) {
        for (auto obj : msg->objects) {
            objectStateMap[obj.object_id] = obj;
        }
        publishDeltaTime(msg);
        publishCollisionHulls(msg);
    }

    void Converter::publishDeltaTime(const automated_driving_msgs::ObjectStateArray::ConstPtr &msg) {
        ros::Duration dt = ros::Time::now() - lastTime;

        //per vehicle with id: publish on that publisher
        for (unsigned int i = 0; i < n; i++) {
            for (auto obj : msg->objects) {
                if (obj.object_id == (i + 1)) {
                    curDtMap[obj.object_id] += dt.toSec();
                    //dt
                    std_msgs::Float64 tmpDtMsg;
                    tmpDtMsg.data = curDtMap[obj.object_id];
                    dtPubs[i].publish(tmpDtMsg);
                    break;
                }
            }
        }

        lastTime = ros::Time::now();
    }

    void Converter::desiredMotionCallback(const simulation_only_msgs::DeltaTrajectoryWithID::ConstPtr &msg) {
        //Convert DeltaTrajectoryWithId to Q^{3,m} in Float64MultiArray
        if (msg->object_id > 0 && msg->object_id <= absTrajPubs.size() && objectStateMap.count(msg->object_id) > 0) {
            double lastdx, lastdy, lastdt;
            bool first = true;
            double cutoff = 10E-3;
            uint offset = 0;
            std_msgs::Float64MultiArray tmpMsg;
            tmpMsg.data.resize(3 * m);
            for (uint i = 0; i < m; i++) {
                //make sure this entry is not the same as the last one
                if (i + offset < msg->delta_poses_with_delta_time.size()) {


                    if (!first) {
                        double dx = std::fabs(
                                lastdx - msg->delta_poses_with_delta_time[i + offset].delta_pose.position.x);
                        double dy = std::fabs(
                                lastdy - msg->delta_poses_with_delta_time[i + offset].delta_pose.position.y);
                        double dt = std::fabs(lastdt - msg->delta_poses_with_delta_time[i + offset].delta_time.toSec());
                        if ((dx + dy + dt) < cutoff) {
                            offset++;
                        }
                    }
                    first = false;

                    //get absolute pose from localization management so that the reference frame is fixed
                    auto curPose = objectStateMap[msg->object_id].motion_state.pose.pose;
                    auto deltaPose = msg->delta_poses_with_delta_time[i + offset].delta_pose;
                    auto absPose = util_localization_mgmt::addDeltaPose(curPose, deltaPose);

                    //x
                    lastdx = msg->delta_poses_with_delta_time[i + offset].delta_pose.position.x;
                    tmpMsg.data[0 * m + i] = absPose.position.x;
                    //y
                    lastdy = msg->delta_poses_with_delta_time[i + offset].delta_pose.position.y;
                    tmpMsg.data[1 * m + i] = absPose.position.y;
                    //dt
                    lastdt = msg->delta_poses_with_delta_time[i + offset].delta_time.toSec();
                    tmpMsg.data[2 * m + i] = lastdt;
                }else{
                    ROS_DEBUG("Not enough elements in desired_motion!");
                    //copy last known
                    tmpMsg.data[0 * m + i] = tmpMsg.data[0 * m + (i-1)];
                    tmpMsg.data[1 * m + i] = tmpMsg.data[1 * m + (i-1)];
                    tmpMsg.data[2 * m + i] = tmpMsg.data[1 * m + (i-1)];
                }
            }
            absTrajPubs[msg->object_id - 1].publish(tmpMsg);

        }
    }

    Rect::Rect(shape_msgs::Mesh mesh) : rightBot(mesh.vertices[0].x, mesh.vertices[0].y),
                                        leftBot(mesh.vertices[1].x, mesh.vertices[1].y),
                                        leftTop(mesh.vertices[2].x, mesh.vertices[2].y),
                                        rightTop(mesh.vertices[3].x, mesh.vertices[3].y) {}


    void Rect::rotateAndTranslate(double xoff, double yoff, double rotation) {
        rightBot.rotate(rotation);
        leftBot.rotate(rotation);
        leftTop.rotate(rotation);
        rightTop.rotate(rotation);

        rightBot.translate(xoff, yoff);
        leftBot.translate(xoff, yoff);
        leftTop.translate(xoff, yoff);
        rightTop.translate(xoff, yoff);
    }

    void Point::rotate(double rotation) {
        double newX = x * cos(rotation) - y * sin(rotation);
        double newY = x * sin(rotation) + y * cos(rotation);

        x = newX;
        y = newY;
    }

    void Point::translate(double xoff, double yoff) {
        x += xoff;
        y += yoff;
    }


}
