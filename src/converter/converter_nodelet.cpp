/* (c) https://github.com/MontiCore/monticore */
#include "converter.hpp"
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

namespace emam_converter {

class ConverterNodelet : public nodelet::Nodelet {

    virtual void onInit();
    boost::shared_ptr<Converter> m_;
};

void ConverterNodelet::onInit() {
    m_.reset(new Converter(getNodeHandle(), getPrivateNodeHandle()));
}

}

PLUGINLIB_DECLARE_CLASS(emam_converter,
                        ConverterNodelet,
                        emam_converter::ConverterNodelet,
                        nodelet::Nodelet);
