/* (c) https://github.com/MontiCore/monticore */
#include "converter.hpp"

int main(int argc, char* argv[]) {

    ros::init(argc, argv, "converter_node");

    emam_converter::Converter converter(ros::NodeHandle(), ros::NodeHandle("~"));

    ros::spin();
    return 0;
}
