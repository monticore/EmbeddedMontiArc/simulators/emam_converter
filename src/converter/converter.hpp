/* (c) https://github.com/MontiCore/monticore */
#pragma once

#include <geodesy/utm.h>
#include <geographic_msgs/GeoPoint.h>
#include <ros/ros.h>
#include <string>
#include <cmath>
#include <regex>
#include <automated_driving_msgs/MotionState.h>
#include <automated_driving_msgs/ObjectStateArray.h>
#include <simulation_only_msgs/DeltaTrajectoryWithID.h>
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float64.h"
#include <struct_msgs/ba_struct_Rectangle.h>
#include <simulation_utils/util_planner.hpp>
#include <simulation_utils/util_localization_mgmt.hpp>
#include <dynamic_reconfigure/DoubleParameter.h>
#include <dynamic_reconfigure/Reconfigure.h>
#include <dynamic_reconfigure/Config.h>
#include "emam_converter/ConverterParameters.h"

namespace emam_converter {
    class Point {
    public:
        Point(double x, double y) : x(x), y(y) {}

        double x, y;
        void rotate(double rotation);
        void translate(double xoff,double yoff);
    };

    class Rect {
    public:
        Rect(Point rightBot, Point leftBot, Point leftTop, Point rightTop) : rightBot(rightBot),
                                                                             leftBot(leftBot),
                                                                             leftTop(leftTop),
                                                                             rightTop(rightTop) {}

        Rect(shape_msgs::Mesh mesh);

        //bool overlapsWith(Rect& b);
        Point rightBot, leftBot, leftTop, rightTop;

        void rotateAndTranslate(double xoff, double yoff, double rotation);
    };


    class Converter {
    public:
        Converter(ros::NodeHandle, ros::NodeHandle);
    private:

        ConverterParameters params_;

        //Sub and Pub
        const unsigned int m = 10;
        const unsigned int n = 2;
        const unsigned int k = 10;

        ros::Time lastTime;
        std::map<uint, automated_driving_msgs::ObjectState> objectStateMap;

        ros::Subscriber groundTruthSub;
        ros::Subscriber desiredMotionSub;
        std::map<uint, double> curDtMap;
        std::vector<ros::Subscriber> curVelSub;
        std::vector<ros::Publisher> absTrajPubs;
        std::vector<ros::Publisher> dtPubs;
        std::vector<ros::Publisher> hullPubs;

        void groundTruthCallback(const automated_driving_msgs::ObjectStateArray::ConstPtr &msg);

        void publishDeltaTime(const automated_driving_msgs::ObjectStateArray_<std::allocator<void>>::ConstPtr &msg);

        void curVelCallback(const std_msgs::Float64::ConstPtr &msg, unsigned int id);

        void desiredMotionCallback(const simulation_only_msgs::DeltaTrajectoryWithID_<std::allocator<void>>::ConstPtr &msg);

        void publishCollisionHulls(const automated_driving_msgs::ObjectStateArray_<std::allocator<void>>::ConstPtr &msg);
    };

}
