<!-- (c) https://github.com/MontiCore/monticore -->
# emam_converter
Part of the cooperative Intersection scenario(ROS Simulation Framework + EMAM model).

Converts messages from the ROS Simulation Framework into an EMAM friendly format. Since EMAM does not support lists/arrays of dynamic length, they need to be cut/padded to a static length. Relative coordinate systems are also converted to absolute coordinate systems.

## Parameters
### Topics
* **ground_truth_topic**: contains current position of vehicles. Used to convert relative to absolute coordinate systems.
* **desired_motion_topic**: contains the relative desired trajectories of the vehicles

### Topic templates
The topics for each vehicle are calcualted by replacing {i} with the vehicle_id.

* **abs_trajectory_topic_template**: used to publish the absolute desired trajectories of the vehicles. Dynamic length of the ROS Simulation Framework msg + relative coordinate system are converted 
* **delta_time_topic_template**: used to publish the time since the last execution of the velocity controllers
* **collision_hulls_topic_template**: used to publish collision hulls of the vehicles. Complicated hulls are simpilfied and all values are absolute.
* **current_velocity_topic_template**: used to update the vehicles velocities via the dynamic reconfigure service
